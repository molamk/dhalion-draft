require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios').default;

const PORT = process.env.PORT || 5000;
const HOST = process.env.HOST || '0.0.0.0';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const BODY_PARSER_OPTIONS = {
    inflate: true,
    limit: 1000,
    extended: true
};

app.use(bodyParser.urlencoded(BODY_PARSER_OPTIONS));

app.get('/', (req, res) => {
    res.sendStatus(200);
})

app.post('/hello', (req, res) => {
    res.send(req.body);
});

app.post('/jira', (req, res) => {
    const data = req.body;
    axios.post("https://hooks.slack.com/services/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", {
        text: `timestamp = ${data.timestamp} | id = ${data.issue.id} | key = ${data.issue.key}`
    }).then(() => {
        res.sendStatus(200);
    }).catch(err => {
        console.error(err);
        res.sendStatus(200);
    });
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`)
});
