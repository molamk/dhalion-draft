build_docker_image:
	docker build -t molamk/dhalion-draft .

run_docker_container:
	docker run -p 49160:8080 -d molamk/dhalion-draft

tag_docker_image:
	docker tag a1af3017ac1a molamk/dhalion-draft:latest

push_docker_image:
	docker push molamk/dhalion-draft
